<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\BlogPost;
use AppBundle\Entity\Comment;
use AppBundle\Form\commentType;



class BlogController extends Controller
{

    /**
     * @Route("blog/list", name="blogList", requirements={"page" : "\d+"})
     */
    public function listAction($page = 1)
    {   
        $repository = $this->getDoctrine()->getRepository(BlogPost::class);

        $posts = $repository->findSortedByDate();

        return $this->render('blog/list.html.twig', array(
            "posts" => $posts,
            "page"=> $page));
    }
    
   
    /**
     * @Route("/blog/{id}")
     */
    public function showAction( BlogPost $post, Request $request)
    {
        
    $comment = new Comment();
    
    $form = $this->createForm(CommentType::class, $comment );
    
  
    
    if($form->isSubmitted() && $form->isValid())
                {
                    $comment->setCreatedAt( new \Datetime());
                    $comment->setBlogPost( $post );
    
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($comment);
                    $em->flush();
                }
       
                return $this->render("blog/show.html.twig", array( 'post' => $post , 'form' => $form->createView()));
    
    }
 

}

    



