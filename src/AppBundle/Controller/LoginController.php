<?php


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\BlogPost;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use \Datetime;
    
    
    class LoginController extends Controller

    {
        // @Security("has_role('ROLE_ADMIN')")
        /**
         * @Route("/admin")
         * 
         */
        public function admin(Request $request){
            
            return $this->render('admin/todo.html.twig');
        }

        /**
         * @Route("/admin/posts", name="PostList")
         */
        public function listPosts(Request $request)
        {

            $posts = $this->getDoctrine()->getRepository(BlogPost::class)->findAll();
            
            return $this->render('admin/posts.html.twig', array( 
                'posts'=>$posts,
            ));
        }

        /**
        * @Route("/admin/posts/new", name="new")
        */
        public function postAction(Request $request)
        {
            $post = new BlogPost();

            $form = $this->createFormBuilder($post)
                ->add('titre', TextType::class, array ('label' => 'Titre', 'required'=> false))
                ->add('content', TextareaType::class, array ('label' => 'Content', 'required'=> false))
                ->add('save', SubmitType::class, array ('label'=> 'create'))
                ->getForm();

            $form->handleRequest($request);
    
            if($form->isSubmitted() && $form->isValid())

            {
                 $post = $form->getData();
                 $post->setpublishedAt(new DateTime());

                $em = $this->getDoctrine()->getManager();
                $em->persist($post);
                $em->flush();

                return $this->redirectToRoute('blogList', array('id'=>$post->getId()));

            }

                return $this->render('admin/blog/new.html.twig', [
                'form'=> $form->createView()
            ]);
        }
    }